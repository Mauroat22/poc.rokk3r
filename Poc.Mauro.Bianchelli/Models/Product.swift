//
//  Product.swift
//  Poc.Mauro.Bianchelli
//
//  Created by Mauro on 2/5/18.
//  Copyright © 2018 Mauro. All rights reserved.
//

import Foundation




struct Product: Equatable {
    var name: String
    var stock: Int
    var price: Float
    
    init(name:String,stock: Int, price: Float) {
        self.name = name
        self.stock = stock
        self.price = price
    }
    
    static var products: [Product] = {
        let product1 = Product(name: "Pepper", stock: 2, price: 1.50)
        let product2 = Product(name: "Banana", stock: 19, price: 1.0)
        let product3 = Product(name: "Orange", stock: 3, price: 0.50)
        let product4 = Product(name: "Mango", stock: 9, price: 2.0)
        let product5 = Product(name: "Chair", stock: 54, price: 55.0)
        let product6 = Product(name: "Fork", stock: 4, price: 2.0)
        let product7 = Product(name: "Pencil", stock: 1, price: 1.0)
        let product8 = Product(name: "Water", stock: 5, price: 3.0)
        let product9 = Product(name: "Cofee", stock: 5, price: 3.0)
        let product10 = Product(name: "Salad", stock: 93, price: 4.0)
        let product11 = Product(name: "Shoes", stock: 33, price: 15.0)
        let product12 = Product(name: "T-Shirt", stock: 9, price: 10.0)
        let product13 = Product(name: "Jean", stock: 22, price: 15.0)
        let product14 = Product(name: "Pant", stock: 92, price: 15.0)
        let product15 = Product(name: "Milk", stock: 41, price: 2.0)
        let product16 = Product(name: "Eggs", stock: 29, price: 0.50)
        let product17 = Product(name: "Deodoront", stock: 10, price: 2.0)
        let product18 = Product(name: "Cheesecacke", stock: 85, price: 5.0)
        let product19 = Product(name: "Bakon", stock: 8, price: 1.0)
        let product20 = Product(name: "Bike", stock: 19, price: 300.0)
        
        return [product1,product2,product3, product4, product5, product6, product7, product8, product9,
        product10,product11,product12,product13,product14,product15,product16,product17,product18,product19,product20
        ]
    }()
    
    
    
}


extension Product: Hashable {
    var hashValue: Int {
        return self.name.hashValue
    }
}


