//
//  ShoppingCart.swift
//  Poc.Mauro.Bianchelli
//
//  Created by Mauro on 19/6/18.
//  Copyright © 2018 Mauro. All rights reserved.
//

import Foundation
import RxSwift

class ShoppingCart{
    
    static let sharedInstance = ShoppingCart()
    var products: Variable<[Product]> = Variable([])
    
    func totalCost() -> Float {
        return Float(products.value.reduce(0) {
            total, product in
            return total + product.price
        })
    }
    
    func itemCountString() -> String {
        guard products.value.count > 0 else {
            return ""
        }
        
        let setOfProducts = Set<Product>(products.value)
        let itemStrings: [String] = setOfProducts.map {
            product in
            let count: Int = products.value.reduce(0) {
                total, reduceProduct in
                if product == reduceProduct {
                    return total + 1
                }
                return total
            }
            
            return "\(products): \(count)"
        }
        
        return itemStrings.joined(separator: "\n")
    }
    
}


