//
//  DetailCoordinator.swift
//  Poc.Mauro.Bianchelli
//
//  Created by Mauro on 27/4/18.
//  Copyright © 2018 Mauro. All rights reserved.
//

import Foundation
import UIKit

class ShoppingCartCoordinator{
    var viewController: ShoppingCartViewController?
    let presenter: UINavigationController

    
    init(presenter:UINavigationController) {
        self.presenter = presenter

    }
    
    
    func start() {
        let st = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = st.instantiateViewController(withIdentifier: "ShoppingCartViewController") as? ShoppingCartViewController else {return}
        self.viewController = vc
        presenter.pushViewController(vc, animated: true)
        
    }
}
