//
//  ViewControllerCoordinator.swift
//  Poc.Mauro.Bianchelli
//
//  Created by Mauro on 27/4/18.
//  Copyright © 2018 Mauro. All rights reserved.
//

import Foundation
import UIKit
import Moya


class TableViewControllerCoordinator {
    var presenter: UINavigationController
    var tableViewController: TableViewController?
    var shoppingCoordinator: ShoppingCartCoordinator?

    
    init(presenter: UINavigationController){
        self.presenter = presenter
        
       

        
    }
    
    func start() {
        let st = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = st.instantiateViewController(withIdentifier: "TableViewController") as? TableViewController else{return}
        self.presenter.pushViewController(vc, animated: true)
        self.tableViewController = vc

        
       
        let productsButton =  UIBarButtonItem(title: "Products", style: .done, target: self, action: #selector(showCantAndPrice))
        vc.title = "Shopping Cart"
        vc.navigationItem.setRightBarButton(productsButton, animated: true)

        
        
       
    }

    @objc func showCantAndPrice(){
        let shoppingCartCoordinator = ShoppingCartCoordinator(presenter: self.presenter)
        self.shoppingCoordinator = shoppingCartCoordinator
        shoppingCartCoordinator.start()
    
    }
    
    
}




    



