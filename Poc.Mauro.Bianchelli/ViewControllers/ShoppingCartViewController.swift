//
//  DetailViewController.swift
//  Poc.Mauro.Bianchelli
//
//  Created by Mauro on 27/4/18.
//  Copyright © 2018 Mauro. All rights reserved.
//

import Foundation
import UIKit

class ShoppingCartViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductTableViewCell")
    }
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ShoppingCart.sharedInstance.products.value.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell", for: indexPath)  as? ProductTableViewCell else {return UITableViewCell()}
        cell.nameLabel.text = ShoppingCart.sharedInstance.products.value[indexPath.row].name
        cell.buyButton.setTitle("Delete", for: .normal)
        cell.buyButton.backgroundColor = UIColor.red
        
        cell.delegate = self
        cell.indexPathRow = indexPath.row
        
        return cell
    }
    
}


extension ShoppingCartViewController: BuyProtocol{
    
    func buy(index: Int) {
        
    }
    
    func cancel(index: Int) {
        
        
    }
    
    
}

