//
//  ViewController.swift
//  Poc.Mauro.Bianchelli
//
//  Created by Mauro on 27/4/18.
//  Copyright © 2018 Mauro. All rights reserved.
//

import UIKit
import RxSwift



class TableViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    

    let disposeBag = DisposeBag()
    @IBOutlet weak var tableView: UITableView!
    var products = Product.products
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductTableViewCell")
        setupCartObserver()
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTableViewCell", for: indexPath)  as? ProductTableViewCell else {return UITableViewCell()}
        let products = self.products
       
        cell.nameLabel.text = products[indexPath.row].name
        let price = String(describing: products[indexPath.row].price)
        cell.priceLabel.text = "$\(price)"
        let stock = String(describing: products[indexPath.row].stock)
        cell.stockLabel.text = "cant: \(stock)"
        cell.indexPathRow = indexPath.row
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    private func setupCartObserver() {
        ShoppingCart.sharedInstance.products.asObservable()
            .subscribe({
                product in
                self.navigationItem.rightBarButtonItem?.title = "\(product.element?.count)"
            })
            .disposed(by: disposeBag)
    }
    
    func updateCartButton() {
        self.navigationItem.rightBarButtonItem?.title = "\(ShoppingCart.sharedInstance.products.value.count)"
        
    }

    

}

extension TableViewController: BuyProtocol{
    
    func buy(index: Int) {
        let product = products[index]
        if product.stock >= 2{
            self.products[index].stock = self.products[index].stock - 1
        }else{
            self.products.remove(at: index)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
        ShoppingCart.sharedInstance.products.value.append(product)
        updateCartButton()
       
    }
    
    func cancel(index: Int) {
        
    }
    
    
}

