//
//  ProductTableViewCell.swift
//  Poc.Mauro.Bianchelli
//
//  Created by Mauro on 27/4/18.
//  Copyright © 2018 Mauro. All rights reserved.
//

import UIKit

protocol BuyProtocol:class{
    func buy(index:Int)
    func cancel(index:Int)
}

class ProductTableViewCell: UITableViewCell {

    
    @IBOutlet weak var buyButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var stockLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    weak var delegate: BuyProtocol!
    var indexPathRow = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        buyButton.layer.cornerRadius = 5
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func buyAction(_ sender: Any) {
        if buyButton.titleLabel?.text?.lowercased() == "delete"{
            delegate.cancel(index: indexPathRow)
        }else{
            delegate.buy(index: indexPathRow)
        }
        
    }
    
}
